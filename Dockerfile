FROM centos:7

RUN yum install python3 python3-pip -y
RUN pip3 install flask flask_restful flask-jsonpify

COPY python-api.py opt/python_api/python_api.py
CMD ["python3", "/opt/python_api/python_api.py"]
